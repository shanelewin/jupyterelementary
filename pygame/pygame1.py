from operator import truediv
from unittest import runner
import numpy as np
import pygame

# get the keyboard controls
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

# define screen size constants
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# create the player
class Player(pygame.sprite.Sprite):

    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.Surface((75,25))
        self.surf.fill((255,255,255))
        self.rect = self.surf.get_rect()


# initalize pygmae 
pygame.init()

# create the screen object
screen =pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

# instantiate player
player = Player()

'''
The game loop proceeds as 
1. process user input
2. updates the state of all game objects
3. updates display and audio. 
4. maintains the speed of hte game

The loop ends if the player collides with an obstacle or the player closes the window
'''
running = True

while running:

    # look at every event in the game
    for event in pygame.event.get():
        
        # if escape key is pressed bail out of hte game
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
        
            # also close if esc is presed 
            elif event.type == QUIT:
                running = False

        # fill the screen with black
        screen.fill((0,0,0))

        # Draw the player on the screen 
        #screen.blit(player.surf, (SCREEN_WIDTH/2, SCREEN_HEIGHT/2))
        screen.blit(player.surf, player.rect)
        
        # flip makes it visible
        pygame.display.flip()


        

    

