First we have to install a few things. We're going to put the newest version of python on our computer, and then install anaconda, a popular (and easy to use) data science tool kit. 

Go get the newest python: 
https://www.python.org/downloads/
Dowload 3.xx (3.8.5 for mac is what I downloaded)

First order of business is installing anaconda. 
https://www.anaconda.com/products/individual
Do the Mac or Windows Works fine. You do you.

Now lets setup a directory where we're going to do our work. 
ON a mac, open the terminal by typing command + spacebar, and then type terminal and hit return.

Conda create -n schoolforsnakes python=3
Type yes to any should you proceed questions. 

Now we have to setup some stuff. 
	1. Activate your new repository using conda activate schoolforsnakes
	2. Conda install numpy matplotlib jupyter

Last but not least lets go get the repository where all the homework lives
git clone https://shanelewin@bitbucket.org/shanelewin/schoolforsnakes.git

There's a fair chance that its going to make you install xcode tools.  Say yes.